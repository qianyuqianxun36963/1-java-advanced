package com.ustc.wang.effective.Chapter2.Item2.JavaBeans;// JavaBeans Pattern - allows inconsistency, mandates mutability - Pages 12-13

public class NutritionFactsBuilder {
    // Parameters initialized to default values (if any)
    private int servingSize  = -1;  // Required; no default value
    private int servings     = -1;  //     "     "     "      "
    private int calories     = 0;
    private int fat          = 0;
    private int sodium       = 0;
    private int carbohydrate = 0;

    public NutritionFactsBuilder() { }

    public int getServingSize() {
        return servingSize;
    }

    public NutritionFactsBuilder setServingSize(int servingSize) {
        this.servingSize = servingSize;
        return this;
    }

    public int getServings() {
        return servings;
    }

    public NutritionFactsBuilder setServings(int servings) {
        this.servings = servings;
        return this;
    }

    public int getCalories() {
        return calories;
    }

    public NutritionFactsBuilder setCalories(int calories) {
        this.calories = calories;
        return this;
    }

    public int getFat() {
        return fat;
    }

    public NutritionFactsBuilder setFat(int fat) {
        this.fat = fat;
        return this;
    }

    public int getSodium() {
        return sodium;
    }

    public NutritionFactsBuilder setSodium(int sodium) {
        this.sodium = sodium;
        return this;
    }

    public int getCarbohydrate() {
        return carbohydrate;
    }

    public NutritionFactsBuilder setCarbohydrate(int carbohydrate) {
        this.carbohydrate = carbohydrate;
        return this;
    }

    public static void main(String[] args) {
        NutritionFactsBuilder cocaCola = new NutritionFactsBuilder();
        cocaCola.setServingSize(240);
        cocaCola.setServings(8);
        cocaCola.setCalories(100);
        cocaCola.setSodium(35);
        cocaCola.setCarbohydrate(27);
    }
}