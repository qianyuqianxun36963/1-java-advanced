package com.ustc.wang.effective.fileUtil;

import org.junit.Test;
import java.io.*;

public class FileCombine {
    int lineNum = 1;
    int subLineSize = 10000;
    String outputFileHead = "D:\\temp\\0531\\";
    String inputFilePath = "D:\\temp\\0531\\0531";
    Writer outputFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileHead+lineNum+".txt"), "UTF-8"));

    public FileCombine() throws UnsupportedEncodingException, FileNotFoundException {
    }

    @Test
    public void test() throws IOException {
        combineFiles(inputFilePath);
    }

    public void combineFiles(String path) throws IOException {
        try {
            File file = new File(path);
            String[] subFiles = file.list();
            for (String subFile : subFiles) {
                String fileType = subFile.substring(subFile.lastIndexOf(".")+1);
                if (fileType.equals("py")||fileType.equals("jar")||fileType.equals("gif")) {
                    continue;
                } else {
                    String child = path + File.separator + subFile;
                    File childFile = new File(child);
                    if (childFile.isDirectory()) {
                        combineFiles(child);
                    } else {
                        writeLineToFile("**********-----" + child + "-----********** \r\n");
                        BufferedReader bis = new BufferedReader(new InputStreamReader(new FileInputStream(childFile), "UTF-8"));
                        String line;
                        while ((line = bis.readLine()) != null) {
                            writeLineToFile(line + "\r\n");
                        }
                        bis.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeLineToFile(String line) throws IOException {
        if(lineNum % subLineSize==0){
            outputFile.flush();
            outputFile.close();
            outputFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileHead+lineNum+".txt"), "UTF-8"));
        }
        outputFile.write(line);
        lineNum++;
    }
}
