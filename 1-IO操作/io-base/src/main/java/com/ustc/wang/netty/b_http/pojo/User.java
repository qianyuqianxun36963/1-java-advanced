package com.ustc.wang.netty.b_http.pojo;

import java.util.Date;

public class User {
    private String userName;

    private String method;

    private Date date;

    public User() {
    }

    public User(String userName, String method, Date date) {
        this.userName = userName;
        this.method = method;
        this.date = date;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", method='" + method + '\'' +
                ", date=" + date +
                '}';
    }

    public String getUserName() {
        return userName;
    }

    public User setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getMethod() {
        return method;
    }

    public User setMethod(String method) {
        this.method = method;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public User setDate(Date date) {
        this.date = date;
        return this;
    }
}
