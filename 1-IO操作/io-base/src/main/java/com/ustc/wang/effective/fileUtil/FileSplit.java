package com.ustc.wang.effective.fileUtil;

import org.junit.Test;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileSplit {
    String srcFilePath = "D:\\temp\\0611\\code.txt";

    @Test
    public void test() throws IOException {
        splitFiles(srcFilePath);
    }

    public void splitFiles(String path) throws IOException {
        BufferedReader inputFile = new BufferedReader(new InputStreamReader(new FileInputStream(srcFilePath),"UTF-8"));
        String line = inputFile.readLine();
        BufferedWriter outputFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("K:\\testfile\\temp.txt")));
        while(line!=null){
            Pattern pattern = Pattern.compile("\\*{10}[\\w\\W]*\\*{10}");
            Matcher matcher = pattern.matcher(line.trim());
            if(matcher.matches()){
                outputFile.close();//老的输出文件关掉
                String dirPath = line.replaceAll("\\*","").replaceAll("/n","");
                mkdir(dirPath);
                outputFile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dirPath),"UTF-8"));
            }else{
                outputFile.write(line+"\n");
            }
            line = inputFile.readLine();
        }
        outputFile.close();
    }

    public void mkdir(String path) throws IOException {
        path = path.trim();
        if(path.endsWith("\\")){path = path.substring(0,path.length()-2);}
        File file = new File(path);
        if(file.exists()){
            System.out.println("路径已存在：" + path);
        }else{
            if(file.isDirectory()){
                file.mkdirs();
            }else {
                File parent = new File(file.getParent());
                parent.mkdirs();
                file.createNewFile();
            }
        }
    }
}
