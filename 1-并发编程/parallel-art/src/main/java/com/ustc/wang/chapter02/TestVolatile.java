package com.ustc.wang.chapter02;

import org.junit.Test;

/**
 * @ClassName TestVolatile
 * @Description
 * @Author wangyajun
 * @Date 2019/6/2 23:51
 * @Version 1.0
 **/
public class TestVolatile {
    boolean status = false;

    /**
     * 状态切换为true
     */
    public void changeStatus(){
        status = true;
    }

    /**
     * 若状态为true，则running。
     */
    public void run(){
        if(status){
            System.out.println("running....");
        }
    }

    static TestVolatile testVolatile = new TestVolatile();
    @Test
    public void test(){
        new Thread(){
            public void run(){
                testVolatile.changeStatus();
            }
        }.start();
        new Thread(){
            public void run(){
                testVolatile.run();
            }
        }.start();
    }

}
