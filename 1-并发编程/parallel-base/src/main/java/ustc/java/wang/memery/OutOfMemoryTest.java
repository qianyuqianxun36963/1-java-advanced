package ustc.java.wang.memery;

import org.junit.Test;

import java.util.Date;
import java.util.HashMap;  
import java.util.Map;  

public class OutOfMemoryTest {
	@Test
    public void test(){
        Map<Integer,Date> map=new HashMap<Integer, Date>();  
        for (int i = 0; i < 600000000; i++) {  
            map.put(i, new Date());  
        }  
    }  
}
