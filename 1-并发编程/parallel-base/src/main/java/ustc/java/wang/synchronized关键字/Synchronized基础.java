package ustc.java.wang.synchronized关键字;

import org.junit.Test;

public class Synchronized基础 {

    public void method() {
        //尽量不要用String常量作为锁。
        //synchronized (new String("字符串常量")) {
        synchronized ("字符串常量") {
            try {
                while(true){
                    System.out.println("当前线程 : "  + Thread.currentThread().getName() + "开始");
                    Thread.sleep(10);
                    System.out.println("当前线程 : "  + Thread.currentThread().getName() + "结束");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void test() {
        Thread t1 = new Thread(new Runnable() {
            public void run() {
                method();
            }
        },"t1");
        Thread t2 = new Thread(new Runnable() {
            public void run() {
                method();
            }
        },"t2");

        t2.start();
        t1.start();
    }

}
